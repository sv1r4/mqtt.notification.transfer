### mqtt.notification.transfer

##### Gitlab CI
[![pipeline status](https://gitlab.com/sv1r4/mqtt.notification.transfer/badges/master/pipeline.svg)](https://gitlab.com/sv1r4/mqtt.notification.transfer/commits/master)

##### AppCenter
[![Build status](https://build.appcenter.ms/v0.1/apps/22a957e4-3b96-414e-82e6-76fb62928484/branches/master/badge)](https://appcenter.ms)