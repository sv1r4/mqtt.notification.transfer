﻿using Microsoft.Extensions.Configuration;

namespace mqtt.notification.transfer.android.configuration
{
    public class PreferencesConfigurationSource:IConfigurationSource
    {
        public IConfigurationProvider Build(IConfigurationBuilder builder)
        {
            return new PreferencesConfigurationProvider();
        }
    }
}