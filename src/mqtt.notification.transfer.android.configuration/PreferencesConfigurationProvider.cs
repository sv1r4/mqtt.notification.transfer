﻿using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using AndroidX.Preference;
using Microsoft.Extensions.Configuration;
using Xamarin.Essentials;

namespace mqtt.notification.transfer.android.configuration
{
    public class PreferencesConfigurationProvider:ConfigurationProvider
    {
        private PreferenceChangeListener _listener;
        public override void Load()
        {
            Load(false);

            _listener = new PreferenceChangeListener();
            _listener.SetProvider(this);
            PreferenceManager.GetDefaultSharedPreferences(Application.Context).RegisterOnSharedPreferenceChangeListener(_listener);
        }

        public void Load(bool reload)
        {
            if (reload)
            {
                Data = new Dictionary<string, string>();
            }

            foreach (var preferenceKey in PreferenceManager
                                              .GetDefaultSharedPreferences(Application.Context)
                                              .All
                                              ?.Select(p => p.Key)
                                          ?? Enumerable.Empty<string>())
            {
                Data.Add(NormalizeKey(preferenceKey),
                    Preferences.ContainsKey(preferenceKey) ? Preferences.Get(preferenceKey, null) : null);
            }

            OnReload();
        }

        private static string NormalizeKey(string key)
        {
            return key.Replace("__", ConfigurationPath.KeyDelimiter);
        }


        public class PreferenceChangeListener:Java.Lang.Object, ISharedPreferencesOnSharedPreferenceChangeListener
        {
            private PreferencesConfigurationProvider _provider;

            public void SetProvider(PreferencesConfigurationProvider provider)
            {
                _provider = provider;
            }
            public void OnSharedPreferenceChanged(ISharedPreferences sharedPreferences, string key)
            {
                _provider?.Load(true);
            }
        }
    }


}