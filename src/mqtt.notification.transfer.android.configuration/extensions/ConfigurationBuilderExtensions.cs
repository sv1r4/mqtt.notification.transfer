﻿using Microsoft.Extensions.Configuration;

namespace mqtt.notification.transfer.android.configuration.extensions
{
    public static class ConfigurationBuilderExtensions
    {
        public static IConfigurationBuilder AddAppPreferences(this IConfigurationBuilder builder)
        {
            builder.Add(new PreferencesConfigurationSource());
            return builder;
        }
    }
}