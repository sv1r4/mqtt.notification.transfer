﻿using System;
using System.Threading.Tasks;

namespace mqtt.notification.transfer.mqtt.core
{
    public interface IMqttAdapter:IAsyncDisposable
    {
        Task PublishAsync(string topic, string message);
    }
}