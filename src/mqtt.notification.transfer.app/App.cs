﻿using System;
using Android.App;
using Android.Runtime;
using Android.Util;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using mqtt.notification.transfer.android.configuration.extensions;
using mqtt.notification.transfer.app.config;
using mqtt.notification.transfer.app.scheduler;
using mqtt.notification.transfer.mqtt.core;
using mqtt.notification.transfer.mqtt.mqttnet;
using mqtt.notification.transfer.mqtt.mqttnet.config;

namespace mqtt.notification.transfer.app
{
    [Application]
    public class App : Application
    {
        private static readonly string TAG = "App";
        private static readonly string AppCenterSecret = "95ccbf2e-dcb8-46d7-a22a-9a821f6bec29";
        public App(IntPtr handle, JniHandleOwnership transfer) : base(handle, transfer)
        {
 
        }
 
        public override void OnCreate()
        {
            InitializeServices();

            InitAppCenter();
 
            base.OnCreate(); 
        }

        private static void InitAppCenter()
        {
            AppCenter.Start(AppCenterSecret,
                typeof(Analytics), typeof(Crashes));
        }
 
        public static IServiceProvider ServiceProvider { get; set; }
 
        private static void InitializeServices()
        {
            var config = new ConfigurationBuilder()
                .AddAppPreferences()
                .Build();

            var services = new ServiceCollection();
            services.AddOptions();

            ConfigOptions<MqttConfig>(services, config);
            ConfigOptions<AppConfig>(services, config);
            services.AddTransient<IMqttAdapter, MqttNetAdapter>();

            ServiceProvider = services.BuildServiceProvider();
            
            LogOptionsChange<AppConfig>(ServiceProvider);
            LogOptionsChange<MqttConfig>(ServiceProvider);
        }

        private static void ConfigOptions<T>(IServiceCollection services, IConfiguration config) where T : class, new()
        {
            services.Configure<T>(config.GetSection(typeof(T).Name));
            services.AddTransient<IDelayedJobScheduler, DelayedJobScheduler>();
            services.AddSingleton<IOptionsMonitor<T>, OptionsMonitor<T>>();
        }

        private static void LogOptionsChange<T>(IServiceProvider serviceProvider) where T : class, new()
        {
            var name = typeof(T).Name;
            serviceProvider.GetRequiredService<IOptionsMonitor<T>>().OnChange(
                v => { Log.Debug(TAG, $"{name} changed"); });
        }
    }
}