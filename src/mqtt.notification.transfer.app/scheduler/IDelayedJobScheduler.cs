﻿using System;
using Android.App.Job;
using Android.OS;

namespace mqtt.notification.transfer.app.scheduler
{
    public interface IDelayedJobScheduler
    {
        void DelayJob<T>(int id, TimeSpan delay, PersistableBundle extras) where T:JobService;
        void Cancel(int id);
    }
}