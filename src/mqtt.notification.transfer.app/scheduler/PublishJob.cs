﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.App.Job;
using Android.Util;
using Microsoft.Extensions.DependencyInjection;
using mqtt.notification.transfer.mqtt.core;

namespace mqtt.notification.transfer.app.scheduler
{
    [Service(Name = "mqtt.notification.transfer.app.scheduler.PublishJob", 
        Permission = "android.permission.BIND_JOB_SERVICE")]
    public class PublishJob : JobService
    {
        private const string TAG = "mqtt.notification.transfer.app.scheduler.PublishJob";
        private CancellationTokenSource _cancellationTokenSource= new CancellationTokenSource();
        public override bool OnStartJob(JobParameters jobParams)
        {
            _cancellationTokenSource= new CancellationTokenSource();
            Task.Run(async () =>
            {
                await using (var mqtt = App.ServiceProvider.GetService<IMqttAdapter>())
                {
                    try
                    {
                        var topic = jobParams.Extras.GetString("topic");
                        var msg = jobParams.Extras.GetString("message");
                        await mqtt.PublishAsync(topic, msg);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(TAG, ex.ToString());
                    }
                }
                JobFinished(jobParams, false);
            }, _cancellationTokenSource.Token);
            return true;  
        }

        public override bool OnStopJob(JobParameters jobParams)
        {
            _cancellationTokenSource.Cancel();
            // we don't want to reschedule the job if it is stopped or cancelled.
            return false; 
        }
    }
}