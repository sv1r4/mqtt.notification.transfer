﻿using System;
using Android.App;
using Android.App.Job;
using Android.Content;
using Android.OS;

namespace mqtt.notification.transfer.app.scheduler
{
    public class DelayedJobScheduler:IDelayedJobScheduler
    {
        public void DelayJob<T>(int id, TimeSpan delay, PersistableBundle extras) where T:JobService
        {
            var javaClass = Java.Lang.Class.FromType(typeof(T));
            var componentName = new ComponentName(Application.Context, javaClass);
            var jobInfoBuilder = new JobInfo.Builder(id, componentName)
                .SetMinimumLatency((long)delay.TotalMilliseconds)
                .SetExtras(extras);

            var jobInfo = jobInfoBuilder.Build();
            var jobScheduler = (JobScheduler)Application.Context.GetSystemService(Context.JobSchedulerService);
            
            jobScheduler.Schedule(jobInfo);
        }

        public void Cancel(int id)
        {
            var jobScheduler = (JobScheduler)Application.Context.GetSystemService(Context.JobSchedulerService);
            jobScheduler.Cancel(id);
        }
    }
}