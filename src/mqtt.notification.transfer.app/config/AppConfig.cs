﻿namespace mqtt.notification.transfer.app.config
{
    public class AppConfig
    {
        public string HomeSsid { get; set; }
        public string NotificationTopic { get; set; }
        public int DelayNotifySec { get; set; }
    }
}