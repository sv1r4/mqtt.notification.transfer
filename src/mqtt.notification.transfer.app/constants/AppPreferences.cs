﻿using mqtt.notification.transfer.app.config;
using mqtt.notification.transfer.mqtt.mqttnet.config;

namespace mqtt.notification.transfer.app.constants
{
    public static class AppPreferences
    {
        public static readonly string MqttHost = $"{nameof(MqttConfig)}:{nameof(MqttConfig.Host)}";
        public static readonly string MqttPort = $"{nameof(MqttConfig)}:{nameof(MqttConfig.Port)}";
        public static readonly string HomeSsid = $"{nameof(AppConfig)}:{nameof(AppConfig.HomeSsid)}";
        public static readonly string NotificationTopic = $"{nameof(AppConfig)}:{nameof(AppConfig.NotificationTopic)}";
        public static readonly string DelayNotifySec = $"{nameof(AppConfig)}:{nameof(AppConfig.DelayNotifySec)}";
    }
}