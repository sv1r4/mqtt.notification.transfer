using System;
using Android.App;
using Android.Net.Wifi;
using Android.OS;
using Android.Service.Notification;
using Android.Util;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using mqtt.notification.transfer.app.config;
using mqtt.notification.transfer.app.scheduler;
using Org.Json;

// ReSharper disable ConvertToUsingDeclaration

namespace mqtt.notification.transfer.app.services
{
    [Service(Label = "NotificationService",Permission = "android.permission.BIND_NOTIFICATION_LISTENER_SERVICE")]
    [IntentFilter (new[] { "android.service.notification.NotificationListenerService" })]
    // ReSharper disable once UnusedMember.Global
    public class NotificationService:NotificationListenerService
    {
        private static readonly string TAG = nameof(NotificationService);
        private readonly Func<AppConfig> _appConfigFunc;
        private readonly IDelayedJobScheduler _scheduler;

        public NotificationService()
        {
            _appConfigFunc = ()=>App.ServiceProvider.GetRequiredService<IOptionsMonitor<AppConfig>>().CurrentValue;
            _scheduler = App.ServiceProvider.GetRequiredService<IDelayedJobScheduler>();
        }

        public override void OnNotificationRemoved(StatusBarNotification sbn)
        {
            base.OnNotificationRemoved(sbn);
            _scheduler.Cancel(sbn.Id);
        }

        public override void OnNotificationPosted(StatusBarNotification sbn)
        {
            if (!IsHomeWifi())
            {
                Log.Debug(TAG, "not home wifi skip transfer notification");
                return;
            }

            var topic = _appConfigFunc().NotificationTopic;

            if (string.IsNullOrWhiteSpace(topic))
            {
                Log.Warn(TAG, "Mqtt topic is empty. skip transfer notification");
                return;
            }

            if (sbn.IsOngoing)
            {
                Log.Debug(TAG, "Ongoing notification. skip transfer notification");
                return;
            }
            

            //todo resolve hack (prevent double gmail notification)
            try
            {
                if (sbn.Key.Split('|')[2] == "0")
                {
                    return;
                }
            }
            catch
            {
                //ignore
            }
            
            var msg = BuildMessageFromNotification(sbn);

            var extras = new PersistableBundle();
            extras.PutString("topic", topic);
            extras.PutString("message", msg);
            _scheduler.DelayJob<PublishJob>(sbn.Id, TimeSpan.FromSeconds(_appConfigFunc().DelayNotifySec), extras);
        }

        private static string BuildMessageFromNotification(StatusBarNotification sbn)
        {
            var extras = sbn.Notification?.Extras?? Bundle.Empty;
            var jo = new JSONObject();
            jo.Put("PackageName", sbn.PackageName);
            jo.Put("Key", sbn.Key);
            jo.Put("Tag", sbn.Tag);
            foreach (var key in extras.KeySet())
            {
                jo.Put(key, JSONObject.Wrap(extras.Get(key)));
            }

            var msg = jo.ToString(1);
            return msg;
        }

        private bool IsHomeWifi()
        {
            using (var wifiManager = (WifiManager) Application.Context.GetSystemService(WifiService))
            {
                var currentWifiSsid = wifiManager.ConnectionInfo.SSID.Replace("\"", "");
                var homeSsid = _appConfigFunc().HomeSsid;
                return string.Equals(currentWifiSsid, homeSsid, StringComparison.InvariantCultureIgnoreCase);
            }
            
        }

    }

   
}