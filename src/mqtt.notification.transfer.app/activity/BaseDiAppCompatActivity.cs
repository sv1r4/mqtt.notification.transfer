﻿using System;
using Android.OS;
using Android.Support.V7.App;
using Microsoft.Extensions.DependencyInjection;

namespace mqtt.notification.transfer.app.activity
{
    public abstract class BaseDiAppCompatActivity:AppCompatActivity
    {
        private readonly IServiceProvider _serviceProvider;
        protected IServiceProvider ScopedServiceProvider { get; private set; }
        private IServiceScope _serviceScope;

        protected BaseDiAppCompatActivity()
        {
            _serviceProvider = App.ServiceProvider;
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            _serviceScope = _serviceProvider.CreateScope();
            ScopedServiceProvider = _serviceScope.ServiceProvider;
        }

        protected override void OnDestroy()
        {
            try
            {
                _serviceScope.Dispose();
            }
            finally
            {
                base.OnDestroy();
            }
        }
    }
}