﻿using System;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Views;
using Android.Widget;
using mqtt.notification.transfer.app.constants;
using Xamarin.Essentials;

namespace mqtt.notification.transfer.app.activity
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : BaseDiAppCompatActivity
    {
        private EditText _mqttPort;
        private EditText _mqttHost;
        private EditText _homeSsid;
        private EditText _notificationTopic;
        private EditText _delayNotifySec;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            InitPreferences();

            
            CheckPermission(Android.Manifest.Permission.AccessFineLocation);
        }

        private void InitPreferences()
        {
            _mqttHost = FindViewById<EditText>(Resource.Id.mqttHost);
            _mqttPort = FindViewById<EditText>(Resource.Id.mqttPort);
            _homeSsid = FindViewById<EditText>(Resource.Id.homeSsid);
            _notificationTopic = FindViewById<EditText>(Resource.Id.notificationTopic);
            _delayNotifySec = FindViewById<EditText>(Resource.Id.delayNotifySec);

            InitEditTextPreference(_mqttHost, AppPreferences.MqttHost);
            InitEditTextPreference(_mqttPort, AppPreferences.MqttPort, "1883");
            InitEditTextPreference(_homeSsid, AppPreferences.HomeSsid);
            InitEditTextPreference(_notificationTopic, AppPreferences.NotificationTopic);
            InitEditTextPreference(_delayNotifySec, AppPreferences.DelayNotifySec, "1");
        }

        private void InitEditTextPreference(EditText edit, string key, string defaultValue = "")
        {
            var prefVal = Preferences.Get(key, defaultValue);
            edit.SetText(prefVal, TextView.BufferType.Normal);
            if (string.Equals(prefVal, defaultValue, StringComparison.InvariantCulture)
                && !string.IsNullOrWhiteSpace(defaultValue))
            {
                Preferences.Set(key, defaultValue);
            }
            edit.TextChanged += (sender, args) =>
            {
                var val = args.Text.ToString();
                if (string.IsNullOrWhiteSpace(val))
                {
                    val = defaultValue;
                }
                
                Preferences.Set(key,val);
            };
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        private void CheckPermission(string permission)
        {
            if (ContextCompat.CheckSelfPermission(this, permission) != Android.Content.PM.Permission.Granted)
            {
                ActivityCompat.RequestPermissions(this, new [] { permission }, 0);
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Permission Granted!!!");
            }
        }
	}
}

