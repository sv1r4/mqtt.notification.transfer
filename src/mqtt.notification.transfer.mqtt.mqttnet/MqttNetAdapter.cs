﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using mqtt.notification.transfer.mqtt.core;
using mqtt.notification.transfer.mqtt.mqttnet.config;
using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Client.Options;

namespace mqtt.notification.transfer.mqtt.mqttnet
{
    public class MqttNetAdapter:IMqttAdapter
    {
        private IMqttClient _mqtt;
        private readonly IMqttClientOptions _mqttOptions;

        public MqttNetAdapter(IOptionsMonitor<MqttConfig> mqttConfig)
        {
            _mqtt = new MqttFactory().CreateMqttClient();

            _mqttOptions = new MqttClientOptionsBuilder()
                    .WithClientId($"mqtt.notification.transfer.app-{Guid.NewGuid()}")
                    .WithTcpServer(mqttConfig.CurrentValue.Host, mqttConfig.CurrentValue.Port)
                    .Build();
        }

        public async Task PublishAsync(string topic, string message)
        {
            await ConnectIfRequiredAsync();

            await _mqtt.PublishAsync(new MqttApplicationMessage
            {
                Topic = topic,
                Payload = Encoding.UTF8.GetBytes(message)
            });
        }

        private async Task ConnectIfRequiredAsync()
        {
            if (!_mqtt.IsConnected)
            {
                await _mqtt.ConnectAsync(_mqttOptions);
            }
        }

        public async ValueTask DisposeAsync()
        {
            if (_mqtt?.IsConnected == true)
            {
                try
                {
                    await _mqtt.DisconnectAsync();
                }
                catch
                {
                    //ognore
                }
            }
            _mqtt?.Dispose();
            _mqtt = null;
        }

    }
}