﻿namespace mqtt.notification.transfer.mqtt.mqttnet.config
{
    public class MqttConfig
    {
        public string Host { get; set; }
        public int Port { get; set; } = 1883;
    }
}