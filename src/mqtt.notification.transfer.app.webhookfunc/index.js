const https = require('https')
const projectId = process.env.PROJECT_ID;
const apiToken = process.env.GITLAB_API_TOKEN;
const host = "gitlab.com";
const jobNameSuccess = process.env.CI_JOB_CALLBACK_SUCCESS;
const jobNameFail = process.env.CI_JOB_CALLBACK_FAIL;

function apiRequest(options){

  return new Promise(function(resolve, reject) {
    var req = https.request(options, function(res) {
			// reject on bad status
			if (res.statusCode < 200 || res.statusCode >= 300) {
				return reject(new Error('statusCode=' + res.statusCode));
			}
			// cumulate data
			var body = [];
			res.on('data', function(chunk) {
				body.push(chunk);
			});
			// resolve on end
			res.on('end', function() {
				try {
					body = JSON.parse(Buffer.concat(body).toString());
				} catch(e) {
					reject(e);
				}       
				resolve(body);        
			});
		});
	// reject on request error
	req.on('error', function(err) {
		reject(err);
	});
	req.end();
  });
};

function triggerGitlabJob(sha, jobName){

  apiRequest({
    hostname: host,
    port: 443,
    path: "/api/v4/projects/"+projectId+"/pipelines?sha="+sha,
    method: 'GET',
    headers:{'PRIVATE-TOKEN': apiToken}
  })
  .then(function(pipelines){  
    console.log(pipelines);
    //get latest pipeline if few exists
    var pipelineId = pipelines[0].id;
    for(var i = 1;i<pipelines.length;i++){
      if(pipelineId< pipelines[i].id){
        pipelineId = pipelines[i].id;
      }
    }

    return apiRequest({
      hostname: host,
      port: 443,
      path: "/api/v4/projects/"+projectId+"/pipelines/"+pipelineId+"/jobs",
      method: 'GET',
      headers:{'PRIVATE-TOKEN': apiToken}
    });
  })
  .then(function(jobs){
    console.log(jobs);
    var jobId;
    for(var i =0;i<jobs.length;i++){
      if(jobs[i].name == jobName){
        jobId = jobs[i].id;
        console.log("jobid="+jobId);
        break;
      }
    }
    return apiRequest({
      hostname: host,
      port: 443,
      path: "/api/v4/projects/"+projectId+"/jobs/"+jobId+"/play",
      method: 'POST',
      headers:{'PRIVATE-TOKEN': apiToken}
    });
  })
  .then(body=>{
    console.log(body);
  });

};


exports.appcenterToGitlabCIHook = (req, res) => {
  let body = req.body;
  console.log("Got request\n"+JSON.stringify(req.body));
  let sha = body.source_version;
  let jobName = body.build_status == 'Succeeded' ? jobNameSuccess : jobNameFail;
  try{
    triggerGitlabJob(sha, jobName);
    res.status(200).send();
  }catch(error){
    console.log("Trigger job error:\n"+error)
    res.status(500).send();
  }  
};

triggerGitlabJob("50935f7c3a39246af9690285cc9c135d85391d78", jobNameSuccess)